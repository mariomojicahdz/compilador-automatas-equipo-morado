import com.formdev.flatlaf.FlatIntelliJLaf;
import compilerTools.CodeBlock;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import compilerTools.Directory;
import compilerTools.ErrorLSSL;
import compilerTools.Functions;
import compilerTools.Grammar;
import compilerTools.Production;
import compilerTools.TextColor;
import compilerTools.Token;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;
import javax.swing.Timer;


public class Compilador extends javax.swing.JFrame {
    NumeroLinea numeroLinea;
    private String title;
    private Directory directorio;
    private ArrayList<Token> tokens;
    private ArrayList<ErrorLSSL> errors;
    private ArrayList<TextColor> textsColor;
    private Timer timerKeyReleased;
    private ArrayList<Production> identProd;
    private HashMap<String, String> identificadores;
    private boolean codeHasBeenCompiled = false;
    
    /**
     * Creates new form Compilador
     */
    public Compilador() {
        initComponents();
        init();
    }

    private void init() {
        title = "Compilador";
        setLocationRelativeTo(null);
        setTitle(title);
            directorio = new Directory(this, jtpCode, title, ".comp");
        addWindowListener(new WindowAdapter() {// Cuando presiona la "X" de la esquina superior derecha
            @Override
            public void windowClosing(WindowEvent e) {
                directorio.Exit();
                System.exit(0);
            }
        });
        
        numeroLinea = new NumeroLinea(jtpCode); // aqui esta el error del desbordamiento
        jScrollPane1.setRowHeaderView(numeroLinea);//aqui esta el error del desbordamiento
        timerKeyReleased = new Timer((int) (1000 * 0.3), (ActionEvent e) -> {
        colorAnalysis();
        
        
        });
        
        Functions.insertAsteriskInName(this, jtpCode, () -> {
            timerKeyReleased.restart();
        });
       tokens = new ArrayList<>();
        errors = new ArrayList<>();
        textsColor = new ArrayList<>();
        identProd = new ArrayList<>();
        identificadores = new HashMap<>();
        Functions.setAutocompleterJTextComponent(new String[]{"este","abeja","abandonar","abecedario","aún","tiempo","calendario","edad","época","era","fecha","instante","momento","segundo","minuto"
        ,"hora","día","semana","entre semana","fin de semana","mes","año","década","siglo","milenio","ayer","hoy","mañana","amanecer","mediodía","tarde","anochecer","noche","lunes","martes","miércoles","jueves","viernes","sábado","domingo"}, jtpCode, () -> {
            timerKeyReleased.restart();
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnNuevo = new javax.swing.JButton();
        btnEjecutar = new javax.swing.JButton();
        btnCompilar = new javax.swing.JButton();
        btnGuardarC = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtaOutputConsole = new javax.swing.JTextArea();
        btnAbrir = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblTokens = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtpCode = new javax.swing.JTextPane();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Compilador");
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnNuevo.setForeground(new java.awt.Color(255, 255, 255));
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/2.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.setContentAreaFilled(false);
        btnNuevo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNuevo.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/1.png"))); // NOI18N
        btnNuevo.setRequestFocusEnabled(false);
        btnNuevo.setVerifyInputWhenFocusTarget(false);
        btnNuevo.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnNuevo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 70, 70));

        btnEjecutar.setForeground(new java.awt.Color(255, 255, 255));
        btnEjecutar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/run_icon-icons.com_61189.png"))); // NOI18N
        btnEjecutar.setText("Ejecutar");
        btnEjecutar.setContentAreaFilled(false);
        btnEjecutar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEjecutar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/run_icon-icons.com_61189 (1).png"))); // NOI18N
        btnEjecutar.setSelected(true);
        btnEjecutar.setVerifyInputWhenFocusTarget(false);
        btnEjecutar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnEjecutar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEjecutar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEjecutarActionPerformed(evt);
            }
        });
        getContentPane().add(btnEjecutar, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 20, -1, -1));

        btnCompilar.setForeground(new java.awt.Color(255, 255, 255));
        btnCompilar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/s.png"))); // NOI18N
        btnCompilar.setText("Compilar");
        btnCompilar.setContentAreaFilled(false);
        btnCompilar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCompilar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/s1.png"))); // NOI18N
        btnCompilar.setSelected(true);
        btnCompilar.setVerifyInputWhenFocusTarget(false);
        btnCompilar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnCompilar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCompilar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompilarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCompilar, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 20, -1, -1));

        btnGuardarC.setForeground(new java.awt.Color(255, 255, 255));
        btnGuardarC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/g.png"))); // NOI18N
        btnGuardarC.setText("Guardar como");
        btnGuardarC.setContentAreaFilled(false);
        btnGuardarC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGuardarC.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/g2.png"))); // NOI18N
        btnGuardarC.setSelected(true);
        btnGuardarC.setVerifyInputWhenFocusTarget(false);
        btnGuardarC.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnGuardarC.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGuardarC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarCActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardarC, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 20, -1, -1));

        btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/guardar2.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.setContentAreaFilled(false);
        btnGuardar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGuardar.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/guardar.png"))); // NOI18N
        btnGuardar.setSelected(true);
        btnGuardar.setVerifyInputWhenFocusTarget(false);
        btnGuardar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnGuardar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, -1, -1));

        jtaOutputConsole.setEditable(false);
        jtaOutputConsole.setColumns(20);
        jtaOutputConsole.setRows(5);
        jScrollPane2.setViewportView(jtaOutputConsole);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 310, 370, -1));

        btnAbrir.setForeground(new java.awt.Color(255, 255, 255));
        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/folder2.png"))); // NOI18N
        btnAbrir.setText("Abrir");
        btnAbrir.setContentAreaFilled(false);
        btnAbrir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAbrir.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/folder1.png"))); // NOI18N
        btnAbrir.setSelected(true);
        btnAbrir.setVerifyInputWhenFocusTarget(false);
        btnAbrir.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnAbrir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        getContentPane().add(btnAbrir, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, -1, -1));

        tblTokens.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Componente léxico", "Lexema", "[Línea, Columna]"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblTokens.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tblTokens);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 130, 370, 280));

        jScrollPane1.setViewportView(jtpCode);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, 370, 130));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/star.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 850, 420));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        directorio.New();
        clearFields();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
        if (directorio.Open()) {
            colorAnalysis();
            clearFields();
        }
    }//GEN-LAST:event_btnAbrirActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if (directorio.Save()) {
            clearFields();
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnGuardarCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarCActionPerformed
        if (directorio.SaveAs()) {
            clearFields();
        }
    }//GEN-LAST:event_btnGuardarCActionPerformed

    private void btnCompilarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompilarActionPerformed
        if (getTitle().contains("*") || getTitle().equals(title)) {
            if (directorio.Save()) {
                compile();
            }
        } else {
            compile();
        }
    }//GEN-LAST:event_btnCompilarActionPerformed

    private void btnEjecutarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEjecutarActionPerformed
        btnCompilar.doClick();
        if (codeHasBeenCompiled) {
            if (!errors.isEmpty()) {
                JOptionPane.showMessageDialog(null, "No se puede ejecutar el código ya que se encontró uno o más errores",
                        "Error en la compilación", JOptionPane.ERROR_MESSAGE);
            } else {
                CodeBlock codeBlock = Functions.splitCodeInCodeBlocks(tokens, "{", "}", ";");
                System.out.println(codeBlock);
                ArrayList<String> blocksOfCode = codeBlock.getBlocksOfCodeInOrderOfExec();
                System.out.println(blocksOfCode);

            }
        }
    }//GEN-LAST:event_btnEjecutarActionPerformed

    private void compile() {
        clearFields();// limpiar los archivos
        lexicalAnalysis();// para ejecutar el analysis lexico
        fillTableTokens(); //para llenar la tabla de tokens
        syntacticAnalysis(); //para hacer en analisis sintatico
        semanticAnalysis(); //para hacer el analisis semantico
        printConsole();// imprima en la concola
        codeHasBeenCompiled = true; 
    }

    private void lexicalAnalysis() {
        // Extraer tokens
        Lexer lexer;
        try {
            File codigo = new File("code.encrypter");
            FileOutputStream output = new FileOutputStream(codigo);
            byte[] bytesText = jtpCode.getText().getBytes();
            output.write(bytesText);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(new FileInputStream(codigo), "UTF8"));
            lexer = new Lexer(entrada);
            while (true) {
                Token token = lexer.yylex();
                if (token == null) {
                    break;
                }
                tokens.add(token);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("El archivo no pudo ser encontrado... " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Error al escribir en el archivo... " + ex.getMessage());
        }
    }

    private void syntacticAnalysis() {
        /*----------------esta parte imprime en la consola --------------------*/
        Grammar gramatica = new Grammar(tokens, errors);
        /*Eliminacion de errores*/
        gramatica.delete(new String[]{"ERROR","ERROR_1","ERROR_2"},1);
        
        /*Agrupación de valores*/ /* La expreción regular solamente se puede usar una vez*/
        gramatica.group("VALOR","(Numero | Color)",true);
        
        /*Declaración de variables*/
        gramatica.group("VARIABLE","Tipo_dato IDENTIFICADOR Operador_asignado",true);
        gramatica.group("VARIABLE","Tipo_dato Operador_asignado",true,
                2, "error sintático {}: falta el identificador en la variable [#,%]");
        
        gramatica.finalLineColumn(); // busca el error que se encuentra  hasta el final  en la linea
        
        gramatica.group("VARIABLE", "Tipo_dato IDENTIFICADOR Operador_asignado",3,
                "error sintáctico {}: falta el valor en la declaración [#, %]");
        
        gramatica.initialLineColumn(); // busca el error que se encuentra  hasta el inixial en la linea
        
        /*Eliminación de tipos de datos y operadores de asignación*/
        gramatica.delete("Tipo_dato", 4,
                "Error sintatico {}: el tipo de dato no está en una declaración [#, %]");
        
        gramatica.delete("Operador_asignado", 5,
                "Error sintatico {}: el operador de asignación no está en una declaración [#, %]");
        
        
        /*Agrupar de identificadores y definición de párametros*/
        gramatica.group("VALOR","IDENTIFICADOR", true);
        gramatica.group("PARAMETROS","VALOR(Coma VALOR)+");
        
        /*Agrupacion de Funciones*/
        gramatica.group("FUNCION", "(Movimiento | Pintar | Detener_pintar | Tomar |"+
                "Lanzar_moneda | Ver | Detener_repetir)", true);
        
        gramatica.group("FUNCION_COMP","FUNCION Parentesis_abierto (VALOR | PARAMETROS)? Parentesis_cerrado", true);
        gramatica.group("FUNCION_COMP","FUNCION (VALOR | PARAMETROS)? Parentesis_cerrado", true,
                6, "error sintáctico {}: falta el paréntesis que abre en la función [#, %]");
        
        gramatica.finalLineColumn();
        
        gramatica.group("FUNCION_COMP","FUNCION Parentesis_abierto (VALOR | PARAMETROS)", true,
                7, "error sintáctico {}: falta el paréntesis que abre en la función [#, %]");
        
        //Función Comp = función completa
        gramatica.initialLineColumn(); // busca el error que se encuentra  hasta el inixial en la linea
        
        gramatica.delete("FUNCION", 8,"Error sintático {}: La función no esta declarada correctamente [#, %]");
        
        gramatica.loopForFunExecUntilChangeNotDetected(()->{ // sirve para detectar las agrupaciones y se va a detener hasta que no haya niguna
        gramatica.group("EXP_LOGICA", "(FUNCION_COMP | EXP_LOGICA)(Operador_logico (FUNCION_COMP| EXP_LOGICA))+");
        gramatica.group("EXP_LOGICA", "Parentesis_abierto (EXP_LOGICA | FUNCION_COMP) Parentesis_cerrado");
       
        });
       
        /* Eliminación de operadores logico¨*/
        gramatica.delete("Operador_logico", 10,
                "error sintáctico {}: el operador logico no está contenido en una expresión");
        
        /*agrupación de exp. logicas como valor y parametros */
        gramatica.group("VALOR","EXP_LOGICA");
        gramatica.group("PARAMETROS","VALOR(COMA VALOR)+");
        
        /* AGRUPACIÓN DE ESTRUCTURAS DE CONTROL*/
        
        gramatica.group("ESTRUCTURA_CONTROL","(Repetir | Estructura_SI)");
        gramatica.group("ESTRUCTURA_CONTROL_COMP","ESTRUCTURA_CONTROL Parentesis_abierto Parentesis_cerrado");
        gramatica.group("ESTRUCTURA_CONTROL","EST_CONTROL (VALOR | PARAMETROS)");
        gramatica.group("ESTRUCTURA_CONTROL_COMP","ESTRUCTURA_CONTROL Parentesis_abierto (VALOR | PARAMETROS)Parentesis_cerrado");
        //gramatica.group("EXP_LOGICA", "(FUNCION_COMP) (Operador_logico FUNCION_COMP)+");
        
        /*Eliminación de estructuras de control incompletas*/
        gramatica.delete("ESTRUCTURA_CONTROL",11,
                "eeror sintático {}: la estructura de control no está declarada correctamente [#, %]");
        
        /*Eliminación de parentesis */
//        gramatica.delete(new String[]{"Parentesis_abierto","Parentesis_cerrado"})
//        
        
        /* Mostrar gramáticas */  //se crean lo toquen de la consola para que puedan a parecer
        gramatica.show();
    }

    private void semanticAnalysis() {
    }

    private void colorAnalysis() {
        /* Limpiar el arreglo de colores */
        textsColor.clear();
        /* Extraer rangos de colores */
        LexerColor lexerColor;
        try {
            File codigo = new File("color.encrypter");
            FileOutputStream output = new FileOutputStream(codigo);
            byte[] bytesText = jtpCode.getText().getBytes();
            output.write(bytesText);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(new FileInputStream(codigo), "UTF8"));
            lexerColor = new LexerColor(entrada);
            while (true) {
                TextColor textColor = lexerColor.yylex();
                if (textColor == null) {
                    break;
                }
                textsColor.add(textColor);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("El archivo no pudo ser encontrado... " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Error al escribir en el archivo... " + ex.getMessage());
        }
        Functions.colorTextPane(textsColor, jtpCode, new Color(40, 40, 40));
    }

    private void fillTableTokens() {
        tokens.forEach(token -> { // recorrera cada token
            Object[] data = new Object[]{token.getLexicalComp(), token.getLexeme(), "[" + token.getLine() + ", " + token.getColumn() + "]"};
            Functions.addRowDataInTable(tblTokens, data); // estos se van a imprimir por como estan ordenados
            // y los imprime 
        });
    }

    private void printConsole() {
        int sizeErrors = errors.size();
        if (sizeErrors > 0) {
            Functions.sortErrorsByLineAndColumn(errors);
            String strErrors = "\n";
            for (ErrorLSSL error : errors) {
                String strError = String.valueOf(error);
                strErrors += strError + "\n";
            }
            jtaOutputConsole.setText("Compilación terminada...\n" + strErrors + "\nLa compilación terminó con errores...");
        } else {
            jtaOutputConsole.setText("Compilación terminada...");
        }
        jtaOutputConsole.setCaretPosition(0);
    }

    private void clearFields() {
        Functions.clearDataInTable(tblTokens);
        jtaOutputConsole.setText("");
        tokens.clear();
        errors.clear();
        identProd.clear();
        identificadores.clear();
        codeHasBeenCompiled = false;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
       
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Compilador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Compilador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Compilador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Compilador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(new FlatIntelliJLaf());
            } catch (UnsupportedLookAndFeelException ex) {
                System.out.println("LookAndFeel no soportado: " + ex);
            }
            new Compilador().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnCompilar;
    private javax.swing.JButton btnEjecutar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnGuardarC;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jtaOutputConsole;
    private javax.swing.JTextPane jtpCode;
    private javax.swing.JTable tblTokens;
    // End of variables declaration//GEN-END:variables
}
