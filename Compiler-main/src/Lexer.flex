import compilerTools.Token;

%%
%class Lexer
%type Token
%line
%column
%{
    private Token token(String lexeme, String lexicalComp, int line, int column){
        return new Token(lexeme, lexicalComp, line+1, column+1);
    }
%}
/* Variables básicas de comentarios y espacios */
TerminadorDeLinea = \r|\n|\r\n
EntradaDeCaracter = [^\r\n]
EspacioEnBlanco = {TerminadorDeLinea} | [ \t\f]
ComentarioTradicional = "/*" [^*] ~"*/" | "/*" "*"+ "/"
FinDeLineaComentario = "//" {EntradaDeCaracter}* {TerminadorDeLinea}?
ContenidoComentario = ( [^*] | \*+ [^/*] )*
ComentarioDeDocumentacion = "/**" {ContenidoComentario} "*"+ "/"

/* Comentario */
Comentario = {ComentarioTradicional} | {FinDeLineaComentario} | {ComentarioDeDocumentacion}

/* Identificador */
Letra = [A-Za-zÑñ_ÁÉÍÓÚáéíóúÜü]
Digito = [0-9]
Identificador = {Letra}({Letra}|{Digito})*

/* Número */
Numero = 0 | [1-9][0-9]*
%%

/* Comentarios o espacios en blanco */
{Comentario}|{EspacioEnBlanco} { /*Ignorar*/ }

/*Identificador*/
\${Identificador} {return token(yytext(), "IDENTIFICADOR", yyline, yycolumn);}

/*Tipos de dato*/
número |
numero |
Kevin  |
Mario  |
Daniel |
Jovany |
Ricardo |
color { return token(yytext(), "Tipo_dato", yyline, yycolumn);}


/*Número*/
{Numero} { return token(yytext(), "Numero", yyline, yycolumn);}


/*Colores*/
#[{Letra}| {Digito}]{6} { return token(yytext(), "Color", yyline, yycolumn);}

/*Operadores de agrupación*/
"(" { return token(yytext(), "Parentesis_abierto", yyline, yycolumn);}
")" { return token(yytext(), "Parentesis_cerrado", yyline, yycolumn);}
"{" { return token(yytext(), "Llave_A", yyline, yycolumn);}
"}" { return token(yytext(), "Llave_C", yyline, yycolumn);}
"[" { return token(yytext(), "Corchete_abierto", yyline, yycolumn);}
"]" { return token(yytext(), "Corchete_Cerrado", yyline, yycolumn);}


/*Signos de Puntuación*/
"," { return token(yytext(), "Coma", yyline, yycolumn);}
":" { return token(yytext(), "Punto_coma", yyline, yycolumn);}

/*Operador de asignado*/
--> { return token(yytext(), "Operador_asignado", yyline, yycolumn);}

/*Movimiento*/
adelante |
atrás |
izquierda |
derechs |
norte |
sur |
este |
oeste { return token(yytext(), "Movimiento", yyline, yycolumn);}
 
/* Pintar */
pintar { return token(yytext(), "Pintar", yyline, yycolumn);}

/*Deter Pintar*/
detenerpintar { return token(yytext(), "Detener_pintar", yyline, yycolumn);}

/*Tomar*/
tomar |
poner { return token(yytext(), "Tomar", yyline, yycolumn);}

/*Lanzar Moneda*/
LanzarMoneda { return token(yytext(), "Lanzar_moneda", yyline, yycolumn);}
 
/* Ver */
izquierdaEsObstaculo |
izquierdaEsClaro |
izquierdaEsBaliza |
izquierdaEsBlanco |
izquierdaEsNegro |
frenteEsObstaculo |
frenteEsClaro |
frenteEsBaliza |
frenteEsBlanco |
frenteEsNegro |
derechaEsObstaculo |
derechaEsClaro |
derechaEsBaliza |
derechaEsBlanco | 
derechaEsNegro { return token(yytext(), "Ver", yyline, yycolumn); }

/* Repetir */
repetir |
repetirMientras { return token(yytext(), "Repetir", yyline, yycolumn); }

/* Detener repetir */
interrumpir  { return token(yytext(), "Detener_repetir", yyline, yycolumn); }

/*Estructura si¨*/
si |
sino  { return token(yytext(), "Estructura_SI", yyline, yycolumn); }

/*Operadores lógicos*/
"&" |
"|"  { return token(yytext(), "Operador_logico", yyline, yycolumn); }

/*Final*/
final { return token(yytext(), "Final", yyline, yycolumn); }

//Número erróneo
0{Numero} {/*Ignorar*/}
//Identificador erróneo
{Identificador} {/*Ignorar*/}

/*---------------------------C++---------------------------------------------¨*/


/* Marcador de inicio de algoritmo */
( "inicio" | "main" ) { return token(yytext(), "Inicio_algoritmo", yyline, yycolumn);}

/* Marcador de fin de algoritmo */
( "fin" | "end" ) { return token(yytext(), "Fin_algoritmo", yyline, yycolumn);}

/* Tipos de datos */
int |
double |
float |
char |
string |
entero |
doble |
flotante |
caracter |
cadena { return token(yytext(), "TIPO_DATO", yyline, yycolumn);}

/* Estructuras de control */
si |
if |
else |
sino |
encaso |
case |
switch |
elegir { return token(yytext(), "Estructura_control", yyline, yycolumn);}

/* Estructuras de iteracion */
do |
hacer |
while |
mientras |
for |
para { return token(yytext(), "Estructura_iteracion", yyline, yycolumn);}

/* Funcion escribir */
cout |
escribir { return token(yytext(), "Funcion_Escribir", yyline, yycolumn);}

/* Funcion leer */
cin |
leer { return token(yytext(), "leer", yyline, yycolumn);}

/* Comillas */
\" { return token(yytext(), "comilla", yyline, yycolumn);}

/* Dos puntos */
( ":" ) { return token(yytext(), "Dos_puntos", yyline, yycolumn);}

/* Operador Igual */
( ":=" | "=" ) { return token(yytext(), "Igual", yyline, yycolumn);}

/* Operador Diferente */
( "!=" ) { return token(yytext(), "Diferente", yyline, yycolumn);}

/* Operador Suma */
( "+" ) { return token(yytext(), "Suma", yyline, yycolumn);}

/* Operador Resta */
( "-" ) { return token(yytext(), "Resta", yyline, yycolumn);}

/* Operador Multiplicacion */
( "*" ) { return token(yytext(), "Multiplicación", yyline, yycolumn);}

/* Operador Division */
( "/" ) { return token(yytext(), "Divicion", yyline, yycolumn);}

/* Operador Punto */
( "." ) { return token(yytext(), "Punto", yyline, yycolumn);}

/* Operador Salida*/
( "<<" ) { return token(yytext(), "Salida", yyline, yycolumn);}

/* Operador Division */
( ">>" ) { return token(yytext(), "Entrada", yyline, yycolumn);}

/* Operador Mod */
( "%" ) { return token(yytext(), "Mod", yyline, yycolumn);}

/* Libreria */
( "#include<iostream>" ) { return token(yytext(), "Librerias", yyline, yycolumn);}   


. { return token(yytext(), "ERROR", yyline, yycolumn); }